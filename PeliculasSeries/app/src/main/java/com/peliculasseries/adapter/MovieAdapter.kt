package com.peliculasseries.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.peliculasseries.R
import com.peliculasseries.data.IMAGE_BASE
import com.peliculasseries.data.models.Movie
import kotlinx.android.synthetic.main.movie_item.view.*

class MovieAdapter(val movieClick: (Int) -> Unit): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private var movieList: List<Movie> = emptyList<Movie>()

    fun setData(list: List<Movie>) {
        movieList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        )
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movieList[position]
        holder.bindMovie(item)

        holder.itemView.setOnClickListener { movieClick(item.id.toInt()) }
    }

    class MovieViewHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bindMovie(movie: Movie){
            itemView.tv_movie_title.text = movie.title
            itemView.tv_movie_details.text = movie.releaseDate
            Glide.with(itemView).load(IMAGE_BASE + movie.posterPath).into(itemView.iv_movie)
        }
    }
}
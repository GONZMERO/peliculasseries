package com.peliculasseries.data

const val BASE_URL = "https://api.themoviedb.org/3/movie/"
const val API_KEY = "bbf5a3000e95f1dddf266b5e187d4b21"
const val IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"
const val MOVIE_LIST = "popular?api_key=$API_KEY"
const val MOVIE_DETAILS = "{id}?api_key=$API_KEY"
package com.peliculasseries.data.models

import com.squareup.moshi.Json

data class MovieResponse(
    @field:Json(name = "results") val movies: List<Movie>,
)
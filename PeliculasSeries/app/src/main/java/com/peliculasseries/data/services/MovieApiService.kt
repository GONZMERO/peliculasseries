package com.peliculasseries.data.services

import com.peliculasseries.data.BASE_URL
import com.peliculasseries.data.MOVIE_DETAILS
import com.peliculasseries.data.MOVIE_LIST
import com.peliculasseries.data.models.Movie
import com.peliculasseries.data.models.MovieResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private val retrofit: Retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(MoshiConverterFactory.create())
    .build()

interface MovieApiService {

    @GET(MOVIE_LIST)
    fun getMovieList(): Call<MovieResponse>

    @GET(MOVIE_DETAILS)
    fun getMovieDetails(@Path("id") id: Int): Call<Movie>

}

object MovieApi {
    val retrofitService : MovieApiService by lazy {
        retrofit.create(MovieApiService::class.java)
    }
}
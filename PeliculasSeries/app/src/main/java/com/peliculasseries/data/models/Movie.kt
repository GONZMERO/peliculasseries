package com.peliculasseries.data.models

import com.squareup.moshi.Json

class Movie(
    @field:Json(name = "id") val id: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "poster_path") val posterPath: String,
    @field:Json(name = "release_date") val releaseDate: String,
    @field:Json(name = "overview") val overview: String
)
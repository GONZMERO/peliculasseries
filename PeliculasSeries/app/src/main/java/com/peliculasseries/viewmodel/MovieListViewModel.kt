package com.peliculasseries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.peliculasseries.data.models.Movie
import com.peliculasseries.data.models.MovieResponse
import com.peliculasseries.data.services.MovieApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieListViewModel : ViewModel() {

    val movieList = MutableLiveData<List<Movie>>()

    fun getMovieList() {
        val call = MovieApi.retrofitService.getMovieList()

        call.enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                response.body()?.movies?.let { movie ->
                    movieList.postValue(movie)
                }
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                call.cancel()
            }

        })
    }

}
package com.peliculasseries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.peliculasseries.data.models.Movie
import com.peliculasseries.data.services.MovieApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilmDetailsViewModel : ViewModel() {

    val filmDetail = MutableLiveData<Movie>()

    fun getFilmDetail(id: Int){
        val call = MovieApi.retrofitService.getMovieDetails(id)

        call.enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                response.body()?.let { movie ->
                    filmDetail.postValue(movie)
                }
            }

            override fun onFailure(call: Call<Movie>, t: Throwable) {
                call.cancel()
            }

        })
    }

}
package com.peliculasseries.ui.filmdetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.peliculasseries.data.IMAGE_BASE
import com.peliculasseries.databinding.ActivityFilmDetailsBinding
import com.peliculasseries.viewmodel.FilmDetailsViewModel

class FilmDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFilmDetailsBinding

    private lateinit var viewModel: FilmDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilmDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(FilmDetailsViewModel::class.java)

        initUI()
    }

    private fun initUI(){
        val id = intent.extras?.get("id") as Int

        viewModel.getFilmDetail(id)

        viewModel.filmDetail.observe(this, Observer { movie ->
            Glide.with(this).load(IMAGE_BASE + movie.posterPath).into(binding.ivMovieDetails)
            binding.tvTitle.text = movie.title
            binding.tvReleaseDate.text = movie.releaseDate
            binding.tvOverview.text = movie.overview
        })
    }
}
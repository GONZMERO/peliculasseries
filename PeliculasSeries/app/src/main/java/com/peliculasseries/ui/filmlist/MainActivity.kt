package com.peliculasseries.ui.filmlist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.peliculasseries.adapter.MovieAdapter
import com.peliculasseries.databinding.ActivityMainBinding
import com.peliculasseries.ui.filmdetails.FilmDetailsActivity
import com.peliculasseries.viewmodel.MovieListViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MovieListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MovieListViewModel::class.java)

        initUI()
    }

    private fun initUI(){
        binding.rvListaPeliculas.layoutManager = LinearLayoutManager(this)
        binding.rvListaPeliculas.setHasFixedSize(true)

        viewModel.getMovieList()

        viewModel.movieList.observe(this, Observer { list ->
            (binding.rvListaPeliculas.adapter as MovieAdapter).setData(list)
        })

        binding.rvListaPeliculas.adapter = MovieAdapter{
            val intent = Intent(this, FilmDetailsActivity::class.java)
            intent.putExtra("id", it)
            startActivity(intent)
        }
    }

}